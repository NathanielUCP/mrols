# MRols

<!-- badges: start -->

<!-- badges: end -->

The goal of MRols is to estimate a linear models using parallel
computing

## Installation

You can install the package from GitLab.

``` r
devtools::install_git("https://gitlab.com/nathanielUCP/MRols")
```

## Example

This is a basic example which shows you how to solve a common problem:

``` r
library(MRols)
set.seed(001)
X = data.frame(x1 = rnorm(100),
               x2 = rnorm(100))
y = as.vector(rnorm(100))

paraOLS(X, y)
```
