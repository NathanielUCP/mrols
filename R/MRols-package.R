#' @keywords internal
"_PACKAGE"

# The following block is used by usethis to automatically manage
# roxygen namespace tags. Modify with care!
## usethis namespace: start
## usethis namespace: end
NULL

#' Simulated data.
#'
#' A dataset containing a simulated data for testing the paraOLS function.
#'
#' @format A matrix with 6 columns:
#' \describe{
#'   \item{y}{the dependant}
#'   \item{X1}{a vector of one}
#'   \item{X2}{a vector}
#' }
#' @examples
#' data(data_ols)
#' y = as.vector(data_ols[,1])
#' X = as.data.frame(data_ols[,2:6])
#' paraOLS(X, y)
#'
"data_ols"

#' Edgar Anderson's Iris Data.
#'
#' This famous (Fisher's or Anderson's) iris data set gives the measurements in centimeters of the variables sepal length and width and petal length and width, respectively, for 50 flowers from each of 3 species of iris. The species are Iris setosa, versicolor, and virginica.
#'
#' @format A matrix with 6 columns:
#' \describe{
#'   \item{y}{the dependant}
#'   \item{X1}{a vector of one}
#'   \item{X2}{a vector}
#' }
#' @examples
#' data(data_ols)
#' y = as.vector(data_ols[,1])
#' X = as.data.frame(data_ols[,2:6])
#' paraOLS(X, y)
#'
"iris"
