
Mapper <- function(Xi){
  if(!is.data.frame(Xi) )
    stop('Xi should be a data.frame')

  Qi <- qr.Q(qr(as.matrix(Xi)))
  Ri <- qr.R(qr(as.matrix(Xi)))

  return(list("R" = Ri, "Q" = Qi))
}

# Example

Mapper(res[[1]][[2]])
