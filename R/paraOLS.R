
#' Estime un modèle de régression par parallélisation
#'
#' @param X un data.frame. Il devra contenir un vecteur de 1 si le modèle inclut une constante.
#' @param y un vecteur de données numérique
#' @param k un scalaire indiquant le nombre de branches de parallélisation.
#'
#' @return un objet de classe paraOLS qui est le vecteur des paramètres estimés
#' @export
#'
#' @examples
#' data(data_ols)
#' y = as.vector(data_ols[,1])
#' X = as.data.frame(data_ols[,2:6])
#' paraOLS(X, y)
#'
paraOLS <- function(X, y, k = 10){
  if(!is.data.frame(X) | !is.vector(y) | k<1)
    stop('X should be a data.frame, y a vector and k greater than 1')
  if(nrow(X) != length(y))
    stop('X and y should have the same length')

  res1 = Main2(X, y, k)  # Xi et yi
  res2 = lapply(res1[[1]], Mapper) # k x Ri et Qi

  Rtemp = res2[[1]][[1]]
  for (i in 2:k){
    Rtemp = rbind(Rtemp, res2[[i]][[1]])
  }

  QRtemp = Mapper(as.data.frame(Rtemp))

  nrowRt = dim(QRtemp[[2]])[1]
  Qprime = Main2(as.data.frame(QRtemp[[2]]), 1:nrowRt, k)

  V = list()
  sumV = 0
  for(i in 1:k){
    V[[i]] = Reducer(as.matrix(res2[[i]]$Q),
                     as.matrix(Qprime[[1]][[i]]),
                     as.matrix(res1[[2]][[i]])
                     )
    sumV = sumV + V[[i]]
  }

  beta <- solve(QRtemp[[1]]) %*% sumV
  class(beta) <- "paraOLS"
  return(beta)
}

set.seed(001)
X = data.frame(x1 = rnorm(100),
               x2 = rnorm(100))
y = as.vector(rnorm(100))

paraOLS(X, y)
